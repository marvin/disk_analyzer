# Simple disk analyzer


### Usage:


`./analyzer.py SRC`

SRC - may be device or image file


### Analyzing device

Let's check disk structure on `/dev/sdf`

		% sudo ./analyzer.py /dev/sdf                                                                                                              22:57:16
		parent: 0 partition data: 446-462, type: Linux
		parent: 0 partition data: 462-478, type: Solaris x86 -or- Linux Swap
		55AA


### Analyzing disk image:


In following example we will read disk image from file `example.img` and dump partitions.

	% ./analyzer.py example_disk.img                                                                                                              22:42:05
	parent: 0 partition data: 446-462, type: FAT32, LBA
	parent: 0 partition data: 462-478, type: APT, Atari Partition Table
	Found APT partition at sector 0x40003F (4194367). Prev: 0x0 (0). Next: 0x400690 (4195984)
	mapping_slots:

	partitions:
		 1: 67,  0,   0x4000a6 (     4194470 bytes),       0x64 (         100 bytes),  0, 64
		 2: 67,  0,   0x40010b (     4194571 bytes),       0x64 (         100 bytes),  0, 64
		 3: 67,  0,   0x400170 (     4194672 bytes),       0x64 (         100 bytes),  0, 64
		 4: 67,  0,   0x4001d5 (     4194773 bytes),       0x64 (         100 bytes),  0, 64
		 5: 67,  0,   0x40023a (     4194874 bytes),       0x64 (         100 bytes),  0, 64
		 6: 67,  0,   0x40029f (     4194975 bytes),       0x64 (         100 bytes),  0, 64
		 7: 67,  0,   0x400304 (     4195076 bytes),       0x64 (         100 bytes),  0, 64
		 8: 67,  0,   0x400369 (     4195177 bytes),       0x64 (         100 bytes),  0, 64
		 9: 67,  0,   0x4003ce (     4195278 bytes),       0x64 (         100 bytes),  0, 64
		10: 67,  0,   0x400433 (     4195379 bytes),       0x64 (         100 bytes),  0, 64
		11: 67,  0,   0x400498 (     4195480 bytes),       0x64 (         100 bytes),  0, 64
		12: 67,  0,   0x4004fd (     4195581 bytes),       0x64 (         100 bytes),  0, 64
		13: 67,  0,   0x400562 (     4195682 bytes),       0x64 (         100 bytes),  0, 64
		14: 67,  0,   0x4005c7 (     4195783 bytes),       0x64 (         100 bytes),  0, 64
		15: 67,  0,   0x40062c (     4195884 bytes),       0x64 (         100 bytes),  0, 64

	Found APT partition at sector 0x400690 (4195984). Prev: 0x40003F (4194367). Next: 0x0 (0)
	mapping_slots:

	partitions:
		 1: 67,  0,   0x400692 (     4195986 bytes),       0x64 (         100 bytes),  0, 64
		 2: 67,  0,   0x4006f7 (     4196087 bytes),       0x64 (         100 bytes),  0, 64
		 3: 67,  0,   0x40075c (     4196188 bytes),       0x64 (         100 bytes),  0, 64
		 4: 67,  0,   0x4007c1 (     4196289 bytes),       0x64 (         100 bytes),  0, 64
		 5: 67,  0,   0x400826 (     4196390 bytes),       0x64 (         100 bytes),  0, 64
		 6: 67,  0,   0x40088b (     4196491 bytes),       0x64 (         100 bytes),  0, 64
		 7: 67,  0,   0x4008f0 (     4196592 bytes),       0x64 (         100 bytes),  0, 64
		 8: 67,  0,   0x400955 (     4196693 bytes),       0x64 (         100 bytes),  0, 64
		 9: 67,  0,   0x4009ba (     4196794 bytes),       0x64 (         100 bytes),  0, 64
		10: 67,  0,   0x400a1f (     4196895 bytes),       0x64 (         100 bytes),  0, 64
		11: 67,  0,   0x400a84 (     4196996 bytes),       0x64 (         100 bytes),  0, 64
		12: 67,  0,   0x400ae9 (     4197097 bytes),       0x64 (         100 bytes),  0, 64
		13: 67,  0,   0x400b4e (     4197198 bytes),       0x64 (         100 bytes),  0, 64
		14: 67,  0,   0x400bb3 (     4197299 bytes),       0x64 (         100 bytes),  0, 64
		15: 67,  0,   0x400c18 (     4197400 bytes),       0x64 (         100 bytes),  0, 64
		16: 67,  0,   0x400c7d (     4197501 bytes),       0x64 (         100 bytes),  0, 64
		17: 67,  0,   0x400ce2 (     4197602 bytes),       0x64 (         100 bytes),  0, 64
		18: 67,  0,   0x400d47 (     4197703 bytes),       0x64 (         100 bytes),  0, 64
		19: 67,  0,   0x400dac (     4197804 bytes),       0x64 (         100 bytes),  0, 64

	55AA


