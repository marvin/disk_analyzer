#!/bin/env python
# vim: ts=4 expandtab sw=4 softtabstop=4 nu

"""
    Simple disk analyzer. Reads partition table and prints all entries
"""

import logging
import logging.config
from struct import unpack
from pprint import pprint
import yaml

BOOT_CODE_SIZE = 446
PARTITION_ENTRY_SIZE = 16
SECTOR_SIZE = 512
BOOT_SECTOR_SIZE = 512

PARTITION_TYPES = {
    0x00: "Empty",
    0x01: "FAT12, CHS",
    0x04: "FAT16, 16-32 MB, CHS",
    0x05: "Microsoft Extended (CHS)",
    0x06: "FAT16 (32 MB-2GB CHS)",
    0x07: "NTFS",
    0x0b: "FAT32, CHS",
    0x0c: "FAT32, LBA",
    0x0e: "FAT16, 32 MB-2GB, LBA",
    0x0f: "Microsoft Extended  (LBA)",
    0x11: "Hidden Fat12, CHS",
    0x14: "Hidden FAT16, 16-32 MB, CHS",
    0x16: "Hidden FAT16, 32 MB-2GB, CHS",
    0x1b: "Hidden FAT32, CHS",
    0x1c: "Hidden FAT32, LBA",
    0x1e: "Hidden FAT16, 32 MB-2GB, LBA",
    0x42: "Microsoft MBR, Dynamic Disk",
    0x7f: "APT, Atari Partition Table",
    0x82: "Solaris x86 -or- Linux Swap",
    0x83: "Linux",
    0x84: "Hibernation",
    0x85: "Linux Extended",
    0x86: "NTFS Volume Set",
    0x87: "NTFS Volume SET",
    0xa0: "Hibernation",
    0xa1: "Hibernation",
    0xa5: "FreeBSD",
    0xa6: "OpenBSD",
    0xa8: "Mac OSX",
    0xa9: "NetBSD",
    0xab: "Mac OSX Boot",
    0xb7: "BSDI",
    0xb8: "BSDI swap",
    0xdb: "Recovery Partition",
    0xde: "Dell Diagnostic Partition",
    0xee: "EFI GPT Disk",
    0xef: "EFI System Partition",
    0xfb: "Vmware File System",
    0xfc: "Vmware swap",
}

class Partition():
    """
    | Offset (bytes)	| Field length	| Description                                         |
    | 0x00		        | 1 byte	    | Status or physical drive (bit 7 set is for active   |
                                          or bootable, old MBRs only accept 0x80, 0x00 means  |
                                          inactive, and 0x01–0x7F stand for invalid)          |
    | 0x01		        | 3 bytes	    | CHS address of first absolute sector in partition.  |
                                          The format is described by three bytes, see below.  |
    | 0x04		        | 1 byte	    | Partition type                                      |
    | 0x05		        | 3 bytes	    | CHS address of last absolute sector in partition.   |
                                          The format is described by 3 bytes, see below.      |
    | 0x08		        | 4 bytes	    | LBA of first absolute sector in the partition       |
    | 0x0C		        | 4 bytes	    | Number of sectors in partition                      |

    | CHS address offset | Field length  | Description                                        |
    | 0x01	             | 1 byte        | head (bits 7-0)                                    |
    | 0x02	             | 1 byte        | sector (bits 5–0)
                                           bits 7–6 are high bits of cylinder                 |
    | 0x03               | 1 byte        | cylinder (bits 7-0)                                |

    to calculate actual cylinder use formula:
        cylinder = 256*(sector >> 6) + cylinder

    to calculate actual sector ignore two highest bits:
        sector = sector & 0b00111111

    """

    def __init__(self, data, parent=None):
        """
            Constructor
        """

        logger = logging.getLogger()
        self.parent = parent
        self.flags = unpack('<B', data[0:1])[0]
        self.chs_start = set_chs(data[1:4])
        self.type = unpack('<B', data[4:5])[0]
        self.chs_end = set_chs(data[5:8])

        if self.is_empty:
            self.lba_start = 0
        else:
            self.lba_start = unpack('<L', data[8:12])[0]

        self.size = unpack('<L', data[12:16])[0]


    def get_type(self):
        """
        Returns the text value of the partition type
        """
        return PARTITION_TYPES[self.type]

    def __repr__(self):
        """
            Represent object as formatted string
        """
        return '{}\t{}\t{}\t{}\t{}/{}/{}\t{}/{}/{}'.format(
            self.lba_start+self.parent,
            self.lba_start+self.size+self.parent,
            self.type,
            self.get_type(),
            self.chs_start['cyl'],
            self.chs_start['head'],
            self.chs_start['sec'],
            self.chs_end['cyl'],
            self.chs_end['head'],
            self.chs_end['sec'],

        )

    @property
    def is_bootable(self):
        """
        Returns True if this partition is bootable
        """
        return self.flags == 0x80

    @property
    def is_empty(self):
        """
        Returns True if the partition is empty
        """
        return 'Empty' in self.get_type()

    @property
    def is_extended(self):
        """
        Returns True if the partition is an extended partition
        """
        return 'Extended' in self.get_type()

    @property
    def is_apt(self):
        """
        Returns true if the partition is APT type (Atari Partition Table)
        """
        return self.type == 0x7f


class MBR():
    """
        MBR struct

        | Address	   | Description	       | Size (bytes) | Comments                      |
        | 0x0000 (0)   | Bootstrap code area   | 446          |                               |
        | 0x01BE (446) | Partition entry #1    | 16           |                               |
        | 0x01CE (462) | Partition entry #2    | 16           |                               |
        | 0x01DE (478) | Partition entry #3    | 16           | Unused if Extended partitions |
        | 0x01EE (494) | Partition entry #4	   | 16           | Unused if Extended partitions |
        | 0x01FE (510) | 0x55AA	Boot signature | 2            |                               |
                                                Total size: 446 + 4×16 + 2	512
    """

    def __init__(self, data, parent=0, src=None):
        """
            Constructor
        """
        self._data = data
        self.boot_code = unpack('<{}B'.format(BOOT_CODE_SIZE), data[0:BOOT_CODE_SIZE])
        self.signature = unpack(">H", data[510:])[0]
        self.partitions = {}
        max_entries = 4
        if parent>0:
            # EBR uses only first two partitions
            max_entries = 2
        for i in range(max_entries):
            partition_start = parent+i*PARTITION_ENTRY_SIZE
            partition_data_start = BOOT_CODE_SIZE+i*PARTITION_ENTRY_SIZE
            partition_data_end = partition_data_start+PARTITION_ENTRY_SIZE
            partition = Partition(bytes(data[partition_data_start:partition_data_end]), parent)
            if not partition.is_empty:
                logger.info('parent: %d partition data: %d-%d, type: %s',
                    parent,
                    partition_data_start,
                    partition_data_end,
                    partition.get_type())
                self.partitions[partition_start] = partition
                if partition.is_extended:
                    self.partitions = self.partitions | process_extended(src, partition.lba_start)
                if partition.is_apt:
                    process_apt(src, partition.lba_start)

    def dump(self):
        """
            Dumps RAW data
        """

        return f"{self._data}"

    @property
    def is_valid(self):
        '''
            Returns true if MBR has valid signature
        '''
        logger.info("%X", self.signature)
        return self.signature == 0x55AA


class APTHeader():
    """
        APT Header class
    """
    # pylint: disable=too-many-instance-attributes

    def __init__(self, data):
        """
            Constructor
        """
        self._raw = data
        tmp = unpack('<B3BBBBBII', data)
        self.is_data_low = tmp[0] & 0b10000000
        self.revision = tmp[0] & 0b01100000
        self.metadata_flag = tmp[0] & 0b00010000
        self.mapping_slots = tmp[0] & 0b00000111
        self.signature = bytes(tmp[1:4])
        self.drive_num = tmp[4] & 0b0011
        self.entries = tmp[5]
        self.offset_next = tmp[6]
        self.offset_prev = tmp[7]
        self.next = tmp[8]
        self.prev = tmp[9]


class APTMappingSlots(dict):
    """
        APT mapping slots class
    """

    def __init__(self, data):
        """
            Constructor
        """
        dict.__init__(self)
        for i in range(0, 15):
            partition = APTPartition(data[i*16:(i+1)*16])
            if not partition.is_empty:
                self[i+1] = partition


class APT():
    """
        APT partition class
    """

    def __init__(self, data, offset):
        """
            Constructor
        """
        self._raw = data
        self.header = APTHeader(data[0:16])
        logger.info('Found APT partition at sector 0x%X (%d). Prev: 0x%X (%d). Next: 0x%X (%d)',
            offset, offset, self.header.prev, self.header.prev,
            self.header.next, self.header.next)
        self.partitions = {}
        self.slots = {}
        has_mappings = 0
        if self.header.prev == 0:
            has_mappings = 1
        if has_mappings:
            ### TODO: Check if slots are always in 240b following header
            self.slots = APTMappingSlots(data[16:256])

        # now loop over paritions
        i = 0
        not_empty = True
        while not_empty:
            # read partition chunk
            part_start = (has_mappings)*256+(i+1)*16
            part_end = part_start+16
            if part_start == 512:
                break
            partition = APTPartition(data[part_start:part_end])
            if not partition.is_empty:
                self.partitions[i+1] = partition
            else:
                not_empty = False
            i += 1

    def __repr__(self):
        """
            Represent object as formatted string
        """
        return (
            "mapping_slots:\n\t" +
                "\n\t".join(f"{k:2}: {v}" for k, v in self.slots.items()) + "\n" +
            "partitions:\n\t" +
                "\n\t".join(f"{k:2}: {v}" for k,v in self.partitions.items())+"\n"
            )


class APTPartition():
    """
        APT partition class
    """

    def __init__(self, data):
        """
            Constructor
        """
        tmp = unpack('<BBIIHI', data)
        self.flags = tmp[0]
        self.type = tmp[1]
        self.start_sector = tmp[2]
        self.size = tmp[3]
        self.partition_id = tmp[4]
        self.other = tmp[5]
        self._raw = data

    def dump(self):
        """
            Dumps RAW data
        """
        return self._raw

    @property
    def is_empty(self):
        """
            Returns True if partition is empty
        """
        return self.flags == 0

    def __repr__(self):
        """
            Represent object as formatted string
        """
        return (
            f"{self.flags:2}, {self.type:2}, {self.start_sector:#10x} "
            f"({self.start_sector:12} bytes), {self.size:#10x} ({self.size:12} bytes), "
            f"{self.partition_id:2}, {self.other}")


def process_apt(src, offset):
    """
        Process APT partition
    """
    data = read_chunk(src, offset)
    if data[1:4] == b'APT':
        apt = APT(data, offset)
        pprint(apt)
        if apt.header.next != 0:
            process_apt(src, apt.header.next)


def process_extended(src, offset):
    """
        Process extended partition
    """
    partitions = {}
    ebr_start = 0
    lba_start = 0
    logger.info(
      'Found extended partition! (src: %s ebr_start: %d lba_start: %d offset: %d)',
        src, ebr_start, lba_start, offset)
    data = read_chunk(src, offset).partitions
    for partition in data.keys():
        if not data[partition].is_empty:
            if not data[partition].is_extended:
                partitions[partition] = data[partition]
            else:
                logger.info('--->')
                pprint(data[partition])
                logger.info('--->')
    return partitions


def set_chs(data):
    """
        Decode data as CHS (Cylinder/Head/Sector)
    """
    keys = ['head', 'sec', 'cyl']
    tmp = dict(zip(keys, unpack('<3B', data)))
    tmp['cyl'] = 256*(tmp['sec'] >> 6) + tmp['cyl']
    tmp['sec'] = tmp['sec'] & 0b00111111
    return tmp

def read_cfg(cfg_file='config.yaml'):
    """
        Reads application config from file. Default looks for config.yaml in application directory
    """
    logger = logging.getLogger()
    with open(cfg_file, 'r') as cfg:
        logger.info('Reading config from %s', cfg_file)
        return yaml.safe_load(cfg)

def read_chunk(src, offset, sector_size=512, size=512):
    """
        Reads chunk of data from src (file or device)
        Defaults:
            sector_size = 512
            size = 512
        With default settings it reads one sector
    """
    data = None
#    logger.info('Reading %d bytes from sector 0x%X (%d) [0x%x (%d)]', size, offset,
#        offset, offset*sector_size, offset*sector_size)
    with open(src, 'rb') as disk:
        disk.seek(0)
        disk.seek(offset*sector_size)
        data = disk.read(size)
    return data

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('file', type=str, help='File with MBR data)')
    parser.add_argument('-c', '--config', type=str, help='Config file', default='config.yaml')

    args = parser.parse_args()

    CFG = read_cfg(args.config)
    if 'logging' in CFG:
        logging.config.dictConfig(CFG['logging'])
    else:
        logging.basicConfig(level=eval(level=logging.INFO))
    logger = logging.getLogger()

    mbr = MBR(read_chunk(args.file, 0), parent=0, src=args.file)
    if mbr.is_valid:
        pass
    else:
        logger.error('Invalid MBR!')
        logger.error(mbr.dump())
